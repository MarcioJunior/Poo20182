package br.ucsal.bes20182.ed.fila;

class NOF {
	int valor;
	NOF prox;
}

public class Fila {
	private NOF inicio = null;
	private static int comprimento = 0;

	public void inserir(int valor) {
		NOF novo = new NOF();
		novo.valor = valor;
		if (inicio == null) {
			inicio = novo;
		} else {
			NOF aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}
		comprimento++;
		System.out.println("ESTADO ATUALIZADO! Foi adicionada a senha " + valor + " na fila!");
	}

	public void remover() {
		if (inicio == null) {
			System.out.println("LISTA J� EST� VAZIA!");
		} else {
			NOF aux = inicio;
			System.out.println("\nESTADO ATUALIZADO! Foi removida a senha " + inicio.valor + " na fila!");
			aux = aux.prox;
			inicio = aux;
		}
		comprimento--;

	}

	public void imprimir() {
		if (inicio == null) {
			System.out.println("Lista Vazia!");
		} else {
			NOF aux = inicio;
			while (aux != null) {
				System.out.print(aux.valor + " ");
				aux = aux.prox;
			}

		}
	}

	public boolean contem(int valor) {
		if (inicio == null) {
			return false;
		} else {
			NOF aux = inicio;
			while (aux.prox != null) {
				if (aux.valor == valor) {
					return true;
				}
				aux = aux.prox;
			}
		}
		return false;
	}

	public void comprimento() {
		System.out.println("O comprimento dessa Fila � de " + comprimento + " elementos!");
	}

	public void inverso() {
		Pilha inversa = new Pilha();
		NOF aux = inicio;
		while (aux != null) {
			inversa.push(aux.valor);
			aux = aux.prox;
		}
		inversa.imprimir();

	}

	public void alternar() {
		int seq = 0;
		NOF senhas = inicio;
		NOF norm = inicio;
		NOF pref = inicio;

		Fila alternada = new Fila();
		while (senhas != null) {
			if (norm.valor + seq <= 3 + seq) {
				System.out.println("Senha Normal");
				alternada.inserir(norm.valor);
				seq++;
				norm = norm.prox;
			} else if (norm.valor == senhas.valor - 2) {
				System.out.println("Senha Preferencial");
				alternada.inserir(pref.valor);
				seq++;
				pref = pref.prox;
				alternada.inserir(norm.valor);
				seq++;
				norm = norm.prox;
				alternada.inserir(norm.valor);
				seq++;
				norm = norm.prox;
				alternada.inserir(norm.valor);
				seq++;
				norm = norm.prox;
				
			} else {

			}
			senhas = senhas.prox;
		}
		alternada.imprimir();

	}
}
