package br.ucsal.bes20182.ed.fila;

class NOP {
	int valor;
	NOP prox;
}

public class Pilha {
	NOP topo = null;

	public void push(int valor) {
		NOP novo = new NOP();
		novo.valor = valor;

		if (topo == null) {
			topo = novo;
		} else {
			novo.prox = topo;
			topo = novo;
		}
	}

	public void imprimir() {
		if (topo == null) {
			System.out.println("Pilha Vazia");
		} else {
			NOP aux = topo;
			while (aux != null) {
				System.out.print(aux.valor + " ");
				aux = aux.prox;
			}

		}
	}
}
