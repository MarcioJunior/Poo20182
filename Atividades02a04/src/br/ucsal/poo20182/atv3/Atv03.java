package br.ucsal.poo20182.atv3;

import java.util.Scanner;

public class Atv03 {

	static Scanner sc = new Scanner(System.in);
	static int [] vet = new int [6];
	static int max = 0;

	public static void main(String[] args) {
		obterNum(vet);
		encontrarNumeroMaior(vet);
		exibirMaiorNumero(max);


	}
	public static void obterNum(int [] vet) {
		System.out.println("Informe 5 N�meros Inteiros");
		for (int i = 1 ;i < vet.length;i++) {
			System.out.println("Informe o "+i+" n�mero: ");
			vet [i] = sc.nextInt();
		}

	}
	public static int encontrarNumeroMaior(int []vet) {
		int max = vet[0];
		for (int i = 1; i < vet.length; i++) {
			if (vet[i] > max)
				max = vet[i];
		}
		return max;
	}

	public static void exibirMaiorNumero(int maior) {
		System.out.println("O maior n�mero dentre os informados � "+encontrarNumeroMaior(vet));
	}
}
