package br.ucsal.poo20182.atv3;

import java.util.Scanner;

public class Atv04 {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {

		int tamanho = 0;
		int esc = 0;
		int posi = 0;
		String op =" s" ;
		
		System.out.println("## BEM VINDO A AGENDA MYTH ##");
		//Entrada de dados para tamanho da agenda
		System.out.println("Criar Nova Agenda, Informe o n�mero de contatos que ser�o salvos nesta agenda: ");
		tamanho = sc.nextInt();
		//Declara��o de Vari�veis
		String [] nome = new String [tamanho + 1];
		String [] tel = new String [tamanho + 1];
		int [] ano= new int [tamanho + 1];
		String [] tipo = new String [tamanho + 1];
        		//Falta Criar Date para data de Nacimento
        
		do {
			//Entrada de Dados do Menu 
			System.out.println("O que deseja? \n(1)Incluir Contato \n(2)Excluir Contato\n(3)Listar Contatos\n(4)Pesquisar nome ");     
			esc = sc.nextInt();

			
			switch (esc) {
			case 1://Adicionar novo Contato
				
				System.out.println("Espa�o Reservado Para Contatos "+tamanho);
				
				posi += 1;
				System.out.println("Posi��o Acessada "+ posi);
				//Entrada de Dados
				System.out.println("Novo Contato \n Informe o nome:");
				nome [posi] = sc.next();
				System.out.println("Informe o N�mero de Telefone: ");
				tel [posi] = sc.next();
				System.out.println("Informe o ano de Nascimento: ");
				ano [posi] = sc.nextInt();
				System.out.println("Contato Pessoal ou Profissional? ");
				tipo [posi] = sc.next();
				//Saida de Dados
				System.out.println("Contato Salvo com SUCESSO");
				break;

			case 2://Excluir um Contato
			  //Entrada de Dados
				System.out.println("Qual posi��o do contato a ser deletado? ");
				System.out.println("Se n�o souber a posi��o do contato, consulte a sua lista de contatos com a op��o 3 do menu!");
				posi = sc.nextInt();
				//Processamento
			       for(int i = 0; i < nome.length; i++){
			    	   if (posi == i) {
			    		   nome [i] = null;
			    		   tel [i] = null;
			    		   ano [i] = 0;
			    		   tipo[i] = null;
			    	   }
			       }
				break;
			case 3://Listar Contatos
				
				//Saida de Dados
				for (int i = 1;i < nome.length;i++) {
					System.out.println("Contato "+ i);
					System.out.println("Nome " +nome[i] + "\nTel:. "+ tel[i] + "\nData Nascimento: " + ano [i]);
					System.out.println("");
				}
				break;
			case 4://Buscar Contato
				
				//Entrada de Dados
				String busca = " ";
				System.out.println("Informe o Nome do Contato:");
				busca = sc.next();
				    //Processamento
					for (int i = 0 ; i < nome.length; i ++) {
						if (busca.equals(nome[i])) {
						System.out.println("Nome: "+nome[i]);
						System.out.println("Telefone:. " +tel[i]);
						System.out.println("Ano de Nascimento: "+ ano[i]);
					}
				}
				break;
			default:
				//Saida de Dados
				System.out.println("Op��o Inv�lida, Tente novamente");
			} 
			//Entrada de Dados
			System.out.println("Deseja Voltar ao Menu? ");
			op = sc.next();
		}while (op.equals("s") || op.equals("S")) ;
		
		
	}
}



