package br.ucsal;

class CaixinhaCategoria { // Vamos criar a caixinha
	public String elemento;
	public CaixinhaCategoria proximo;
	public CaixinhaSub ini;
}

class CaixinhaSub {
	public String subElemento;
	public CaixinhaSub prox;
}

public class ListasMultidimensionais {
	private static CaixinhaCategoria inicio;

	public void inserir(String elemento) {
		// Primeiro passo instanciar uma nova caixinha
		CaixinhaCategoria nova = new CaixinhaCategoria();
		// Depois vamos inserir o novo elemento nessa caixinha
		nova.elemento = elemento;
		nova.proximo = null;

		if (inicio == null) { // isso indica que � a primeira caixinha de todo encadeamento
			inicio = nova; // colocar a caixinha no inicio

		} else {// indica que j� tem outra caixinha dentro do encadeamento
			// J� que a h� outra caixinha vamos, criar um auxiliar para n�o perder o inicio,
			// consequentemente toda a lista
			CaixinhaCategoria aux = inicio;
			while (aux.proximo != null) {
				aux = aux.proximo;
			}
			aux.proximo = nova;
		}

	}

	public CaixinhaCategoria buscar(String elementoEntrada) {
		if (inicio == null) {
			return null;
		} else {
			CaixinhaCategoria aux = inicio;
			while (aux.proximo != null && aux.elemento.equals(elementoEntrada)) {
				aux = aux.proximo;
			}
			if (aux.elemento.equals(elementoEntrada)) {
				return aux;
			}

		}
		return null;

	}

	public void inserirItens(String CaixinhaCategoria, String CaixinhaSub) {
		if (inicio == null) {
			System.out.println("VAZIA");
		} else {
			CaixinhaCategoria aux = buscar(CaixinhaCategoria);
			if (aux == null) {
				System.out.println("Categoria Inexistente!");
			} else {
				CaixinhaSub novaSub = new CaixinhaSub();
				novaSub.subElemento = CaixinhaSub;
				if (aux.ini == null) {
					aux.ini = novaSub;
				} else {
					CaixinhaSub auxSub = aux.ini;
					while (auxSub.prox != null) {
						auxSub = auxSub.prox;
					}
					auxSub.prox = novaSub;
				}
			}
		}
	}

	public void imprimirTudo() {
		if (inicio == null) {
			System.out.println("Lista Vazia");
		} else {

			CaixinhaCategoria aux = inicio;

			while (aux != null) {

				System.out.println(aux.elemento + " ");

				if (aux.ini != null) {
					CaixinhaSub auxSub = aux.ini;
					while (auxSub != null) {
						System.out.println("NA PADARIA DO SEU QUINCAS VENDE: " + auxSub.subElemento + " ");
						auxSub = auxSub.prox;
					}

				}
				aux = aux.proximo;
				System.out.println();
			}

		}
	}

}
