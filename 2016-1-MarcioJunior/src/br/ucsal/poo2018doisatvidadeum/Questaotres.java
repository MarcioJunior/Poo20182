package br.ucsal.poo2018doisatvidadeum;

import java.util.Scanner;

public class Questaotres {

	public static void main(String[] args) {
		// Cria��o de Vari�veis
		int nota = 0;
		String conceito = "";
		//Entrada de Dados
		do {
			System.out.println("Informe a nota do aluno: ");
	     	nota = new Scanner (System.in).nextInt();
	     	if (nota < 0 || nota > 100){
	     		System.out.println("Valores fora do Intervalo!");
	     	}
		}while (nota < 0 || nota > 100);
        // Processamento
		if (nota >= 0 && nota <= 49){
			conceito = "Insuficiente";
		}else if (nota > 49 && nota <= 64 ){
			conceito = " Regular";
		}else if (nota >= 65 && nota <= 84){
			conceito = "Bom";
		}else if ( nota >= 85 && nota <=100){
			conceito = "�timo";
		}
		// Sa�da
		System.out.println("O conceito do aluno �: " + conceito);
	}

}
