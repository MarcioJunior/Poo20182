package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaooito {

	public static void main(String[] args) {
		//Declara��o de Vari�veis
		float ht = 0;
		float vh = 0;
		float pd = 0;
		float sb = 0;
		float td = 0;
		float sl = 0;
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
        System.out.println("Informe as horas trabalhadas no m�s: ");
        ht = scanner.nextFloat();
        System.out.println("Informe o valor por hora trabalhada: ");
        vh = scanner.nextFloat();
        System.out.println("Informe o percentual de desconto: ");
        pd = scanner.nextFloat();
        //Processamento
        sb = ht*vh;//Sal�rio Bruto
        td =(pd/100)*sb;//Total de desconto
        sl = sb-td; //Sal�rio L�quido
        //Saida
        System.out.println("Horas Trabalhadas: "+ht);
        System.out.println("Sal�rio Bruto: "+sb);
        System.out.println("Desconto: "+td);
        System.out.println("Sal�rio Liquido: "+ sl);
        
        
	}

}
