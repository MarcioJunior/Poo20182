package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaoquatorze {

	public static void main(String[] args) {
		double a =0;
		double b =0;
		double c = 0;
		double d = 0;
		double auxs1 = 0;
		double auxs2 = 0;
		double auxs3 = 0;
		double auxm1 = 0;
		double auxm2 = 0;
		double auxm3 = 0;
		double auxs4 = 0;
		double auxs5 = 0;
		double auxs6 = 0;
		double auxm4 = 0;
		double auxm5= 0;
		double auxm6 = 0;
		
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe o valor de A: ");
		a = scanner.nextDouble();
		System.out.println("Informe o valor de B: ");
		b = scanner.nextDouble();
		System.out.println("Informe o valor de C: ");
		c = scanner.nextDouble();
		System.out.println("Informe o valor de D: ");
		d = scanner.nextDouble();
		//Processamento
		auxs1= a + b;
		auxs2 = a + c;
		auxs3 = a + d;
		auxm1 = a *b;
		auxm2 = a*c;
		auxm3 = a*d;
		auxs4 = b + c;
		auxs5 = b + d;
		auxs6 = c+d;
		auxm4 = b * c;
		auxm5 = b * d;
		auxm6 = c * d;
		//Saida
		System.out.println("A + B= "+auxs1);
		System.out.println("A+ C= "+auxs2);
		System.out.println("A + D= "+auxs3);
		System.out.println("B + C= "+auxs4);
		System.out.println("B + D= "+auxs5);
		System.out.println("C + D= "+auxs6);
		System.out.println("A * B= "+auxm1);
		System.out.println("A * C= "+auxm2);
		System.out.println("A * D= "+auxm3);
		System.out.println("B * C= "+auxm4);
		System.out.println("B * D= "+auxm5);
		System.out.println("C * D= "+auxm6);
		
        
	}

}
