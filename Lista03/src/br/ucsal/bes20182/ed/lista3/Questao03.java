package br.ucsal.bes20182.ed.lista3;

public class Questao03 {
	// ALUNO : M�rcio Brand�o J�nior
	
	CONVIDADO primeiro = null;

	public void inserirConvidado(String name, String adress, String phone) {
		CONVIDADO novo = new CONVIDADO();
		novo.nome = name;
		novo.endere = adress;
		novo.tel = phone;

		if (primeiro == null) {
			primeiro = novo;
		} else {
			CONVIDADO aux = primeiro;

			while (aux.next != null) {
				aux = aux.next;
			}

			aux.next = novo;
		}
	}

	public void mostrar() {
		System.out.println("############################ LISTA DE CONVIDADOS ##############################\n");
		if (primeiro == null) {
			System.out.println("Lista de convidados est� vazia!");
		} else {
			CONVIDADO aux = primeiro;
			int counter = 1;

			while (aux != null) {
				System.out.println("Convidado " + counter + "Nome: " + aux.nome + "Endere�o: " + aux.endere
						+ "Telefone: " + aux.tel);
				counter++;
				aux = aux.next;
			}
		}
	}

	public void procurar(String info) {
		System.out.println("############################ PROCURAR CONVIDADO ###############################\n");
		if (primeiro == null) {
			System.out.println("Lista de convidados est� vazia!");
		} else {
			CONVIDADO aux = primeiro;
			int counter = 1;

			while (aux != null && aux.nome != info && aux.tel != info) {
				aux = aux.next;
				counter++;
			}

			if (aux == null) {
				System.out.println("O convidado procurado n�o est� cadastrado!");
			} else {
				System.out.println("Convidado " + counter + "Nome: " + aux.nome + "Endere�o: " + aux.endere
						+ "Telefone: " + aux.tel);
			}

		}
	}

	class CONVIDADO {
		public String nome;
		public String endere;
		public String tel;
		public CONVIDADO next;
	}
}
