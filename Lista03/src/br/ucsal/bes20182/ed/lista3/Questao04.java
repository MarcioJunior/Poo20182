package br.ucsal.bes20182.ed.lista3;

public class Questao04 {

	//ALUNO : M�rcio Brand�o J�nior
	Item primeiro = null;

	public void addItem(String description, Integer amount, Double price) {
		Item novo = new Item();
		novo.descricao = description;
		novo.montante = amount;
		novo.preco = price;

		if (primeiro == null) {
			primeiro = novo;
		} else {
			Item aux = primeiro;

			while (aux.prox != null) {
				aux = aux.prox;
			}

			aux.prox = novo;
		}
	}

	public void showItens() {
		System.out.println("LISTA DE COMPRA");
		if (primeiro == null) {
			System.out.println("A lista de compras est� vazia!");
		} else {
			Item aux = primeiro;
			int counter = 1;
			double totalPrice = 0;

			while (aux != null) {
				System.out.println("Item " + counter + " Descri��o: " + aux.descricao + " Quantidade: " + aux.montante
						+ " Pre�o: " + aux.preco);
				totalPrice += aux.preco;
				aux = aux.prox;
				counter++;

			}

			System.out.println("Itens: " + (counter - 1) + " Valor total: " + totalPrice);
		}
	}

	public void contains(String description) {
		if (primeiro == null) {
			System.out.println("A lista de compras est� vazia!");
		} else {
			Item aux = primeiro;
			int counter = 1;

			while (aux != null && !aux.descricao.equals(description)) {
				counter++;
				aux = aux.prox;
			}

			if (aux == null) {
				System.out.printf("O item %s n�o est� na lista!\n", description);
			} else {
				System.out.println("Item " + counter + " Descri��o: " + aux.descricao + " Quantidade: " + aux.montante
						+ " Pre�o: " + aux.preco);
			}
		}
	}

	public void removeItem(String description) {
		if (primeiro == null) {
			System.out.println("A lista est� vazia!");
		} else {
			Item ant = null;
			Item aux = primeiro;

			while (aux != null && !aux.descricao.equals(description)) {
				ant = aux;
				aux = aux.prox;
			}

			if (aux == null) {
				System.out.println("O item a seguir n�o tem na lista: " + description);
			} else if (aux.prox == null) {
				ant.prox = null;
				System.out.println("O item a seguir foi removido: " + description);
			} else if (ant == null) {
				primeiro = aux.prox;
			} else {
				ant.prox = aux.prox;
				System.out.println("O item a seguir foi removido: " + description);
			}
		}
	}

	class Item {
		public String descricao;
		public Integer montante;
		public Double preco;
		public Item prox;
	}
}
