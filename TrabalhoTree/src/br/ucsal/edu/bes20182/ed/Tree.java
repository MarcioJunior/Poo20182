package br.ucsal.edu.bes20182.ed;

import java.util.Scanner;

class No {
	public Integer indice;
	public No esq;
	public No dir;

}

class Funcionario {
	public static Funcionario[] todos = new Funcionario[Menu.limite];
	public Long cpf;
	public String nome;
	public String profissao;

}

class TreeByName {
	static TreeByName porNome = new TreeByName();
	static Scanner sc = new Scanner(System.in);
	static No raizNome = null;

	public static void inserir(Integer idx) {
		No novo = new No();
		novo.indice = idx;

		if (raizNome == null) {
			raizNome = novo;
		} else {
			No aux = raizNome;
			No ant;
			while (true) {
				ant = aux;

				if (Funcionario.todos[idx].nome.compareToIgnoreCase(Funcionario.todos[ant.indice].nome) <= 0) { // esquerda
																													// //
																													// esquerda
					aux = aux.esq;
					if (aux == null) {
						ant.esq = novo;
						return;
					}

				} else { // direita
					aux = aux.dir;
					if (aux == null) {
						ant.dir = novo;
						return;
					}
				}
			}
		}
	}

	public void inOrder(No atual) {
		if (atual != null) {
			inOrder(atual.esq);
			System.out.println();
			System.out.println("NOME: " + Funcionario.todos[atual.indice].nome);
			System.out.println("CPF: " + Funcionario.todos[atual.indice].cpf);
			System.out.println("Profisso: " + Funcionario.todos[atual.indice].profissao);
			inOrder(atual.dir);
		}
	}
}

class TreeByCpf {
	static TreeByCpf porCpf = new TreeByCpf();

	static No raizCpf = null;

	public static void inserir(Integer idx) {
		No novo = new No();
		novo.indice = idx;

		if (raizCpf == null) {
			raizCpf = novo;
		} else {
			No aux = raizCpf;
			No ant;

			while (true) {
				ant = aux;
				if (Funcionario.todos[idx].cpf < Funcionario.todos[ant.indice].cpf) { // ir para esquerda
					aux = aux.esq;
					if (aux == null) {
						ant.esq = novo;
						return;
					}
				} else { // ir para direita
					aux = aux.dir;
					if (aux == null) {
						ant.dir = novo;
						return;
					}
				}
			}
		}
	}

	public static No buscar(long cpf) {
		if (raizCpf == null) {
			System.out.println("Arvore Vazia");
			return null;

		} else {
			No aux = raizCpf; // comea a procurar desde raiz
			while (Funcionario.todos[aux.indice].cpf != cpf) {
				if (cpf < Funcionario.todos[aux.indice].cpf) {
					aux = aux.esq; // caminha para esquerda
				} else {
					aux = aux.dir; // caminha para direita
				}
				if (aux == null) {
					System.out.println("CPF NO CADASTRADO!");
					return null; // encontrou uma folha -> sai
				}
			}
			System.out.println(
					"Atravs do nmero de cpf " + Funcionario.todos[aux.indice].cpf + " foram encontrados os dados: ");
			System.out.println();
			System.out.println("Nome do funcionrio: " + Funcionario.todos[aux.indice].nome);
			System.out.println("Cpf do funcionrio: " + Funcionario.todos[aux.indice].cpf);
			System.out.println("Profisso do funcionrio: " + Funcionario.todos[aux.indice].profissao);
			System.out.println();
			return aux; // terminou o lao while e chegou aqui  pq encontrou
						// item
		}
	}

	public void inOrder(No atual) {
		if (atual != null) {
			inOrder(atual.esq);
			System.out.println();
			System.out.println("NOME: " + Funcionario.todos[atual.indice].nome);
			System.out.println("CPF: " + Funcionario.todos[atual.indice].cpf);
			System.out.println("Profisso: " + Funcionario.todos[atual.indice].profissao);
			inOrder(atual.dir);
		}
	}

	public boolean remover(long v) {
		if (raizCpf == null)
			return false;

		No atual = raizCpf;
		No pai = raizCpf;
		boolean filho_esq = true;

		// ****** Buscando o valor **********
		while (Funcionario.todos[atual.indice].cpf != v) { // enquanto nao encontrou
			pai = atual;
			if (v < Funcionario.todos[atual.indice].cpf) { // caminha para esquerda
				atual = atual.esq;
				filho_esq = true; //  filho a esquerda? sim
			} else { // caminha para direita
				atual = atual.dir;
				filho_esq = false; //  filho a esquerda? NAO
			}
			if (atual == null)
				return false; // encontrou uma folha -> sai
		} // fim lao while de busca do valor

		// **************************************************************
		// se chegou aqui quer dizer que encontrou o valor (v)
		// "atual": contem a referencia ao No a ser eliminado
		// "pai": contem a referencia para o pai do No a ser eliminado
		// "filho_esq":  verdadeiro se atual  filho a esquerda do pai
		// **************************************************************

		// Se nao possui nenhum filho ( uma folha), elimine-o
		if (atual.esq == null && atual.dir == null) {
			if (atual == raizCpf)
				raizCpf = null; // se raiz
			else if (filho_esq)
				pai.esq = null; // se for filho a esquerda do pai
			else
				pai.dir = null; // se for filho a direita do pai
		}

		// Se  pai e nao possui um filho a direita, substitui pela subarvore a direita
		else if (atual.dir == null) {
			if (atual == raizCpf)
				raizCpf = atual.esq; // se raiz
			else if (filho_esq)
				pai.esq = atual.esq; // se for filho a esquerda do pai
			else
				pai.dir = atual.esq; // se for filho a direita do pai
		}

		// Se  pai e nao possui um filho a esquerda, substitui pela subarvore a
		// esquerda
		else if (atual.esq == null) {
			if (atual == raizCpf)
				raizCpf = atual.dir; // se raiz
			else if (filho_esq)
				pai.esq = atual.dir; // se for filho a esquerda do pai
			else
				pai.dir = atual.dir; // se for filho a direita do pai
		}

		// Se possui mais de um filho, se for um av ou outro grau maior de parentesco
		else {
			No sucessor = no_sucessor(atual);
			// Usando sucessor que seria o N mais a esquerda da subarvore a direita do No
			// que deseja-se remover
			if (atual == raizCpf)
				raizCpf = sucessor; // se raiz
			else if (filho_esq)
				pai.esq = sucessor; // se for filho a esquerda do pai
			else
				pai.dir = sucessor; // se for filho a direita do pai
			sucessor.esq = atual.esq; // acertando o ponteiro a esquerda do sucessor agora que ele assumiu
										// a posio correta na arvore
		}

		return true;
	}

	// O sucessor  o No mais a esquerda da subarvore a direita do No que foi
	// passado como parametro do metodo
	public No no_sucessor(No apaga) { // O parametro  a referencia para o No que deseja-se apagar
		No paidosucessor = apaga;
		No sucessor = apaga;
		No atual = apaga.dir; // vai para a subarvore a direita

		while (atual != null) { // enquanto nao chegar no N mais a esquerda
			paidosucessor = sucessor;
			sucessor = atual;
			atual = atual.esq; // caminha para a esquerda
		}
		// *********************************************************************************
		// quando sair do while "sucessor" ser o No mais a esquerda da subarvore a
		// direita
		// "paidosucessor" ser o o pai de sucessor e "apaga" o No que dever ser
		// eliminado
		// *********************************************************************************
		if (sucessor != apaga.dir) { // se sucessor nao  o filho a direita do N que dever ser eliminado
			paidosucessor.esq = sucessor.dir; // pai herda os filhos do sucessor que sempre sero a direita
			// lembrando que o sucessor nunca poder ter filhos a esquerda, pois, ele sempre
			// ser o
			// N mais a esquerda da subarvore a direita do N apaga.
			// lembrando tambm que sucessor sempre ser o filho a esquerda do pai

			sucessor.dir = apaga.dir; // guardando a referencia a direita do sucessor para
										// quando ele assumir a posio correta na arvore
		}
		return sucessor;
	}

}
